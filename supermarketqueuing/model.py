import csv
import random

import matplotlib.pyplot as plt
import numpy as np
from mesa import Agent, Model
from mesa.datacollection import DataCollector
from mesa.space import SingleGrid
from mesa.time import RandomActivation


class WorkerAgent(Agent):
    """
    Class to instantiate agents that can be employed as additional cashiers
    """

    def __init__(self, model):
        super().__init__(self, model)
        self._active = False

    def step(self):
        pass


class ObstacleAgent(Agent):
    """
    Class to instantiate physical and static obstacle such as shelves
    """

    def __init__(self, model):
        super().__init__(self, model)
        self._orientation = "vertical"

    def step(self):
        pass


class CashierAgent(Agent):
    """
    Class to instantiate cashiers active from the beginning of simulation
    """

    def __init__(self, model, active_status=-1):
        super().__init__(self, model)
        self.now_open = True
        self._active_status = active_status
        self.remainToClose = 20
        self.initial = False

        if self.model._queue_type == "Snake":
            self._is_occupied = False

    def step(self):
        """
        Logic to be performed for each agent employed as cashier in each simulation time unit
        """
        pos_0 = self.pos[0] - 1
        pos_1 = 0
        pos = (pos_0, pos_1)

        if self.model._queue_type == "Default":
            if self.model.grid.is_cell_empty(pos) == False:
                self.now_open = False

            if self._active_status == 0:
                if self.model.grid.is_cell_empty(pos) or (
                        self.model.customer_state_count["M"] == 0 and self.model.customer_state_count[
                    "PAY"] == 0 and self.model.agentToSpawn == 0):
                    self.setClose(-1)

        if self.model._queue_type == "Snake":
            if self._is_occupied == True:
                self.now_open = False
            if self._active_status == 0 and self._is_occupied == False:
                self.setClose(-1)

    def setActive(self, active_status):
        """
        Set active a cash register
        """
        self._active_status = active_status
        self.now_open = True

    def setClose(self, active_status):
        """
        Set close a cash register
        """
        self._active_status = active_status
        for worker in self.model.workerPlaces:
            if worker.pos[0] == self.pos[0] + 1:
                worker._active = False

    def initToClose(self, active_status):
        """
        Set a cash register in closing
        """
        if self.now_open == False and self.initial == False:
            self._active_status = active_status
            for worker in self.model.workerPlaces:
                if worker.pos[0] == self.pos[0] + 1:
                    worker._active = True


class CustomerAgent(Agent):
    """
    Class to instantiate customer agents to be spawned in supermarket environment
    """
    num_agents_entry = 0
    num_agents_moving = 0
    num_agents_casher = 0

    def __init__(self, model):
        super().__init__(self, model)
        if self.model._mode != "test":
            self._state_move = "E"
            self.model.customer_state_count["E"] += 1
            self.model.agent_status.append("E")
        else:
            self._state_move = "M"
            self.model.customer_state_count["M"] += 1
            self.model.agent_status.append("M")

        MAX_CHART = 10  # maximum capacity
        self._is_served = False
        self._cart_load = random.randint(1, MAX_CHART)
        self._lock_movement = False
        self._steps = -1
        self._last_move = 100

        # Test attributes
        if self.model._mode == "test":
            self._test_marking = ""

        # Analysis attribute
        self._num_of_steps = 0

        # to lock tests after init
        self.lock_test = False
        if (self.model._queue_type == "Snake"):
            self._snake_movement = "right"
            self._chosen_cash_pos = -1

        self._purchasing = np.random.randint(2, 8)
        while 1:
            self._lanes_list = list(np.random.randint(0, 2, 8))
            if (self._lanes_list[1] == 1):
                coin = np.random.rand()
                if (coin <= 0.5):
                    self._lanes_list[1] = 0
            if (self._lanes_list[0] == 1):
                coin = np.random.rand()
                if (coin <= 0.5):
                    self._lanes_list[0] = 0
            if (self._lanes_list[7] == 1):
                coin = np.random.rand()
                if (coin <= 0.5):
                    self._lanes_list[7] = 0
            # to debug destination use below command tuned
            # self._lanes_list=[1,1,0,0,0,0,0,0]
            if any(self._lanes_list) == 1:
                break
        self._cart_load = self._cart_load * self._lanes_list.count(1)

    def step(self):
        """
        Logic to be performed for each customer agent at each simulation time unit 
        """
        self.model.heatmap[self.pos[1]][self.pos[0]] += 1
        if self._is_served:  # agent is served by a cashier
            if self._cart_load == 0:  # payment complete
                if self.model._queue_type == "Snake":
                    c = [2, 5, 8, 11, 14, 17, 20, 23]
                    position_to_cycle = 0
                    while position_to_cycle < 8:
                        if c[position_to_cycle] == self._chosen_cash_pos:
                            position_in_cashiers = position_to_cycle
                        position_to_cycle += 1
                    self.model.cashiers[position_in_cashiers]._is_occupied = False
                self.model.schedule.remove(self)
                self.model.grid.remove_agent(self)
                self._state_move = "PAYED"
                self.model.customer_state_count["PAYED"] += 1
                self.model.customer_state_count["PAY"] -= 1
                self.model._num_agents_sample += 1
                self.model._sum_tot_steps += self.get_steps()
                self.model._avg_steps = self.model._sum_tot_steps / self.model._num_agents_sample


            else:  # payment is in progress
                self._cart_load -= 1
        else:  # agent is not served
            self.move()

    def get_destination(self):
        """
        Get the target cell for the agent
        """
        cur_pos_grid = int(self.model.grid_matrix[self.pos[1]][self.pos[0]])

        # if I am in corridor
        if (cur_pos_grid <= 6):
            # if in corridor and with index equal to fish shelf
            if (7 - self._lanes_list.index(1) + 1 == 8):
                # if I am not in correct lane then gt correct position near shelf
                if not (3 <= self._dest[0] <= 10 or 18 <= self._dest[0] <= 25):
                    x1 = np.random.randint(3, 11)
                    x2 = np.random.randint(18, 26)
                    coin = np.random.rand()
                    if (coin <= 0.5):
                        self._dest = [x1, 22]
                    else:
                        self._dest = [x2, 22]
                # get the product
                elif (self.pos[1] == 22 and list(self.pos) == self._dest):
                    self._dest = [self._dest[0], self._dest[1] + 1]
                else:
                    self.get_min_position(self._dest)
            # if in corridor and destination on top or bottom shelf
            elif (7 - self._lanes_list.index(1) + 1 == 7):
                # to get object repulsion first go to lane and then go to top or bottom shelf
                if ((8 <= self._dest[1] <= 9 or 21 <= self._dest[1] <= 22) and list(self.pos) == self._dest and (
                        self._dest[0] == 4 or self._dest[0] == 9 or self._dest[0] == 14 or self._dest[0] == 19 or
                        self._dest[0] == 24 or self._dest[0] == 29)):
                    if (self.pos[1] > 19):
                        y = 20
                    else:
                        y = 10
                    self._dest = [self.pos[0], y]
                # go to
                elif (self._dest[1] != 20 or self._dest[1] != 10):
                    rndX = [4, 9, 14, 19, 24, 29]
                    if (self.pos[1] > 19):
                        y = np.random.randint(21, 23)
                    else:
                        y = np.random.randint(8, 10)
                    moneyX = np.random.randint(0, 6)
                    self._dest = [rndX[moneyX], y]
                else:
                    self.get_min_position(self._dest)
            # case corridor and in the correct lane
            elif (cur_pos_grid == (7 - self._lanes_list.index(1) + 1)):
                # not in same Y, then go to correct Y
                if not (self.pos[1] == self._dest[1] and self._dest[1] >= 11 and self._dest[1] <= 20):
                    x = self.pos[0]
                    if (self.model._mode == "test" and self.model._id_test == 3):
                        y = 15
                    else:
                        y = np.random.randint(11, 20)
                    self._dest = [x, y]

            # if not go to correct lane in corridor
            else:
                if (7 - self._lanes_list.index(1) + 1 == 1):
                    x = np.random.randint(1, 2)
                elif (7 - self._lanes_list.index(1) + 1 == 2):
                    x = np.random.randint(6, 8)
                elif (7 - self._lanes_list.index(1) + 1 == 3):
                    x = np.random.randint(11, 13)
                elif (7 - self._lanes_list.index(1) + 1 == 4):
                    x = np.random.randint(16, 18)
                elif (7 - self._lanes_list.index(1) + 1 == 5):
                    x = np.random.randint(21, 23)
                elif (7 - self._lanes_list.index(1) + 1 == 6):
                    x = np.random.randint(26, 28)
                self._dest = [x, self.pos[1]]
        # if i am in shelf
        else:
            # get random X in correct shelf (same Y)
            if (1 <= self._dest[0] <= 2 or 6 <= self._dest[0] <= 7 or 11 <= self._dest[0] <= 12 or 16 <= self._dest[
                0] <= 17 or
                    21 <= self._dest[0] <= 22 or 26 <= self._dest[0] <= 27):
                if (cur_pos_grid == 10 or cur_pos_grid == 11):
                    x = 3
                elif (cur_pos_grid == 20 or cur_pos_grid == 21):
                    x = np.random.choice((range(5, 9, 3)))
                elif (cur_pos_grid == 30 or cur_pos_grid == 31):
                    x = np.random.choice((range(10, 14, 3)))
                elif (cur_pos_grid == 40 or cur_pos_grid == 41):
                    x = np.random.choice((range(15, 19, 3)))
                elif (cur_pos_grid == 50 or cur_pos_grid == 51):
                    if (self.model._mode == "test" and self.model._id_test == 3):
                        x = 20
                    else:
                        x = np.random.choice((range(20, 24, 3)))
                else:
                    x = np.random.choice((range(25, 29, 3)))
                y = self._dest[1]
                self._dest = [x, y]

            # after taken product go to corridor
            else:
                if (cur_pos_grid == 10 or cur_pos_grid == 11):
                    x = np.random.randint(1, 2)
                if (cur_pos_grid == 20 or cur_pos_grid == 21):
                    x = np.random.randint(6, 8)
                if (cur_pos_grid == 30 or cur_pos_grid == 31):
                    x = np.random.randint(11, 13)
                if (cur_pos_grid == 40 or cur_pos_grid == 41):
                    x = np.random.randint(16, 18)
                if (cur_pos_grid == 50 or cur_pos_grid == 51):
                    x = np.random.randint(21, 23)
                if (cur_pos_grid == 60 or cur_pos_grid == 61):
                    x = np.random.randint(26, 28)
                y1 = np.random.randint(21, 23)
                y2 = np.random.randint(8, 10)
                coin = np.random.rand()
                if (self.pos[1] == 15):
                    if coin <= 0.55:
                        self._dest = [x, y1]
                    else:
                        self._dest = [x, y2]
                elif (self.pos[1] == 14):
                    if coin <= 0.60:
                        self._dest = [x, y2]
                    else:
                        self._dest = [x, y1]
                elif (self.pos[1] == 13):
                    if coin <= 0.70:
                        self._dest = [x, y2]
                    else:
                        self._dest = [x, y1]
                elif (self.pos[1] == 12):
                    if coin <= 0.80:
                        self._dest = [x, y2]
                    else:
                        self._dest = [x, y1]
                elif (self.pos[1] == 11):
                    if coin <= 0.90:
                        self._dest = [x, y2]
                    else:
                        self._dest = [x, y1]
                elif (self.pos[1] == 16):
                    if coin <= 0.60:
                        self._dest = [x, y1]
                    else:
                        self._dest = [x, y2]
                elif (self.pos[1] == 17):
                    if coin <= 0.70:
                        self._dest = [x, y1]
                    else:
                        self._dest = [x, y2]
                elif (self.pos[1] == 18):
                    if coin <= 0.80:
                        self._dest = [x, y1]
                    else:
                        self._dest = [x, y2]
                elif (self.pos[1] == 19):
                    if coin <= 0.90:
                        self._dest = [x, y1]
                    else:
                        self._dest = [x, y2]
                if (7 - self._lanes_list.index(1) + 1 == 7):
                    self._dest = [x, y1]

    def move(self):
        """
        Define customer agent movement for current step
        """
        if (self.pos[1] == 0 and self._state_move == "E"):
            # random destination forward or at left
            x = np.random.randint(26, 28)
            y = [np.random.randint(8, 10), np.random.randint(21, 23)]
            coin = np.random.rand()
            if coin <= 0.8:
                self._dest = [x, y[0]]
            else:
                self._dest = [x, y[1]]
            # go in below corridor
            if (7 - self._lanes_list.index(1) + 1 == 6):
                self._dest = [x, y[0]]
            # go on top corridor
            if (7 - self._lanes_list.index(1) + 1 == 8):
                self._dest = [x, y[1]]

        # UNIT TEST
        if (self.model._mode == "test" and self.lock_test == False):
            self.lock_test = True
            self._state_move = "M"
            if (self.model._id_test == 0):
                if (self.pos[0] == 14):
                    self._dest = [20, 8]
                    self._lanes_list = list(np.zeros(len(self._lanes_list)))
                    self._lanes_list[2] = 1
                else:
                    self._dest = [8, 8]
                    self._lanes_list = list(np.zeros(len(self._lanes_list)))
                    self._lanes_list[7] = 1

            if (self.model._id_test == 1):
                if (self.pos[0] == 14):
                    self._dest = [21, self.pos[1]]
                else:
                    self._dest = [7, self.pos[1]]

            if (self.model._id_test == 2):
                self._dest = [21, 8]

            if (self.model._id_test == 3):
                self._lanes_list = list(np.zeros(len(self._lanes_list)))
                self._lanes_list[3] = 1
                self._dest = [21, random.randint(8, 9)]

            if (self.model._id_test == 4):
                self._lanes_list = list(np.zeros(len(self._lanes_list)))
                self._dest = [0, 0]

        new_pos = self.pos

        if (self._state_move == "E"):
            new_pos = (self.pos[0], self.pos[1] + 1)
            if (self.model.grid_matrix[self.pos[1]][self.pos[0]] == '6'):
                self._state_move = "M"
                self.model.customer_state_count["M"] += 1
                self.model.customer_state_count["E"] -= 1

        if (self._state_move == "Q"):
            movement = self.decideMovement()
            if movement == -1:
                go_to_pos = (self.pos[0] - 1, self.pos[1])
                self._last_move = -1
                if (self.model.grid.is_cell_empty(go_to_pos) == False):
                    go_to_pos = (self.pos[0], self.pos[1] + 1)
                    self._last_move = 0
            elif movement == 0:
                go_to_pos = (self.pos[0], self.pos[1] - 1)
                self._last_move = 0
            elif movement == 1:
                go_to_pos = (self.pos[0] + 1, self.pos[1])
                self._last_move = 1
                if (self.model.grid.is_cell_empty(go_to_pos) == False):
                    go_to_pos = (self.pos[0], self.pos[1] + 1)
                    self._last_move = 0
            if go_to_pos[0] < self.model.gridWidth and go_to_pos[1] < self.model.gridHeight:
                if self.model.grid.is_cell_empty(go_to_pos):
                    new_pos = go_to_pos
                else:
                    pass
            else:
                pass

        # state equal to movement from pt A to pt B
        if (self._state_move == "M"):

            # if i am in queue line get Q status
            if ((int(self.model.grid_matrix[self.pos[1]][self.pos[0]]) == 0 or self.pos == (0, 7)) and not (
                    any(self._lanes_list) == 1)):
                if (self.pos == (0, 7)):
                    self._dest = [0, 5]
                if (self.model._queue_type == "Default"):
                    self._state_move = "Q"
                    self.model.customer_state_count["Q"] += 1
                    self.model.customer_state_count["M"] -= 1
                elif (self.pos == (0, 5)):
                    self._state_move = "QS"
                    self.model.customer_state_count["QS"] += 1
                    self.model.customer_state_count["M"] -= 1
            # if purchased terminated
            elif not (any(self._lanes_list) == 1) and self._dest[1] != 5:
                # not arrived in queue lanes
                if (int(self.model.grid_matrix[self.pos[1]][self.pos[0]]) != 0):
                    # not in corridor then go to below corridor
                    if (self._dest[1] > 10):
                        cur_pos_grid = int(self.model.grid_matrix[self.pos[1]][self.pos[0]])
                        if (cur_pos_grid == 10 or cur_pos_grid == 11):
                            x = np.random.randint(1, 2)
                        if (cur_pos_grid == 20 or cur_pos_grid == 21):
                            x = np.random.randint(6, 8)
                        if (cur_pos_grid == 30 or cur_pos_grid == 31):
                            x = np.random.randint(11, 13)
                        if (cur_pos_grid == 40 or cur_pos_grid == 41):
                            x = np.random.randint(16, 18)
                        if (cur_pos_grid == 50 or cur_pos_grid == 51):
                            x = np.random.randint(21, 23)
                        if (cur_pos_grid == 60 or cur_pos_grid == 61):
                            x = np.random.randint(26, 28)
                        # if here then dirst and only purchase is on top or bottom shelf
                        if (3 <= self.pos[0] <= 4 or 8 <= self.pos[0] <= 9 or 13 <= self.pos[0] <= 14 or 18 <= self.pos[
                            0] <= 19 or 23 <= self.pos[0] <= 24 or 28 <= self.pos[0] <= 29):
                            x = self.pos[0] - 2
                        elif (self.pos[0] == 5 or self.pos[0] == 10 or self.pos[0] == 15 or self.pos[0] == 20 or
                              self.pos[0] == 25):
                            x = self.pos[0] + 1
                        else:
                            x = self.pos[0]
                        y = 9
                        self._dest = [x, y]
                    # in corridor then go to point there queue starts
                    elif (self.pos[1] <= 10):
                        if (self.model._queue_type == "Default"):
                            x = np.random.randint(min(self.model.positionCashOpened),
                                                  max(self.model.positionCashOpened) + 1)
                            y = 5
                        else:
                            x = 0
                            y = 7
                        self._dest = [x, y]

            # if arrived in destination point
            elif (list(self.pos) == self._dest and (
                    ((self.model._mode == "test" and self.model._id_test == 3) or self.model._mode != "test"))):
                # if destination is in shelf stay here for 4 step!
                if (10 <= self.pos[1] <= 20 and (
                        self.pos[0] == 3 or self.pos[0] == 4 or self.pos[0] == 5 or self.pos[0] == 8 or self.pos[
                    0] == 9 or self.pos[0] == 10 or self.pos[
                            0] == 13 or self.pos[0] == 14 or self.pos[0] == 15 or self.pos[0] == 18 or self.pos[
                            0] == 19 or self.pos[0] == 20 or self.pos[0] == 23 or self.pos[0] == 24 or
                        self.pos[0] == 25 or self.pos[0] == 28 or self.pos[0] == 29) or (
                        (3 <= self.pos[0] <= 10 or 18 <= self.pos[0] <= 25) and (self.pos[1] == 23))):
                    if (self._purchasing > 0):
                        self._purchasing -= 1
                    else:
                        self._lanes_list[self._lanes_list.index(1)] = 0
                        self._purchasing = np.random.randint(2, 8)
                        # se non ha terminato gli acquisti e il primo acquisto era in cima o sotto lo scaffale
                        if any(self._lanes_list) == 1:
                            if (self.pos[1] == 10 or self.pos[1] == 23):
                                self._dest = [self.pos[0], self.pos[1] - 1]
                            elif (self.pos[1] == 20):
                                self._dest = [self.pos[0], self.pos[1] + 1]
                            else:
                                self.get_destination()
                else:
                    self.get_destination()

            # stuck
            if (11 <= self.pos[1] <= 19 and 10 <= self._dest[1] <= 20 and self.model._id_test != 1 and
                    (0 <= self.pos[0] == 3 and (
                            5 <= self._dest[0] <= 19)
                     or self.pos[0] == 5 and (
                             0 <= self._dest[0] <= 3 or 10 <= self._dest[0] <= 29)
                     or self.pos[0] == 8 and (
                             0 <= self._dest[0] <= 3 or 10 <= self._dest[0] <= 29)
                     or self.pos[0] == 10 and (
                             0 <= self._dest[0] <= 8 or 15 <= self._dest[
                         0] <= 29)
                     or self.pos[0] == 13 and (
                             0 <= self._dest[0] <= 8 or 15 <= self._dest[
                         0] <= 19)
                     or self.pos[0] == 15 and (
                             0 <= self._dest[0] <= 13 or 20 <= self._dest[
                         0] <= 29)
                     or self.pos[0] == 18 and (
                             0 <= self._dest[0] <= 13 or 20 <= self._dest[
                         0] <= 29)
                     or self.pos[0] == 20 and (
                             0 <= self._dest[0] <= 18 or 25 <= self._dest[
                         0] <= 29)
                     or self.pos[0] == 23 and (
                             0 <= self._dest[0] <= 18 or 25 <= self._dest[
                         0] <= 29)
                     or self.pos[0] == 25 and (
                             0 <= self._dest[0] <= 23)
                     or self.pos[0] == 28 and (
                             0 <= self._dest[0] <= 23))):
                self.get_destination()
            # go to updated destination or old destination in otder to precedent if's
            new_pos = self.get_min_position(self._dest)

        # Snake queue movement
        if (self._state_move == "QS"):
            # the agent is the next one to be served
            if self.pos == (0, 3):
                if self._chosen_cash_pos == -1:
                    self.decide_movement_snake()
                if self._chosen_cash_pos != -1:
                    # go down one step - agente ha scelto la cassa
                    new_pos = (self.pos[0], self.pos[1] - 1)

            if self.pos[1] == 2 and self.pos[0] < self._chosen_cash_pos - 1:
                new_pos = (self.pos[0] + 1, self.pos[1])
            if (self.pos[1] == 2 or self.pos[1] == 1) and self.pos[0] == self._chosen_cash_pos - 1:
                new_pos = (self.pos[0], self.pos[1] - 1)
            if (self.pos[1] == 0):
                self._state_move = "PAY"
                self.model.customer_state_count["PAY"] += 1
                if (self.model._queue_type == "Default"):
                    self.model.customer_state_count["Q"] -= 1
                else:
                    self.model.customer_state_count["QS"] -= 1

            if (self.pos == (0, 5)):
                new_pos = (self.pos[0], self.pos[1] - 1)
            if (self.pos[0] == 24 and self.pos[1] == 3):
                self._snake_movement = "left"

            # first snake row
            if (self._snake_movement == "right"):
                if (self.pos[0] < 24 and self.pos[1] == 4):
                    new_pos = (self.pos[0] + 1, self.pos[1])
                elif (self.pos[0] == 24 and self.pos[1] == 4):
                    new_pos = (self.pos[0], self.pos[1] - 1)
            # second snake row
            elif (self._snake_movement == "left"):
                if (self.pos[0] > 0 and self.pos[1] == 3):
                    new_pos = (self.pos[0] - 1, self.pos[1])

        if new_pos[0] < self.model.gridWidth and new_pos[1] < self.model.gridHeight:
            if (self.model.grid.is_cell_empty(new_pos) == True):
                if (self.model._queue_type == "QS" and int(self.model.grid_matrix[self.pos[1]][self.pos[0]]) != 0):
                    self.model.grid.move_agent(self, new_pos)
                elif (self.model._queue_type != "QS"):
                    self.model.grid.move_agent(self, new_pos)

        if (new_pos[1] == 0 and new_pos[0] < self.model.gridWidth - 4):
            self._state_move = "PAY"
            self.model.customer_state_count["PAY"] += 1
            if (self.model._queue_type == "Default"):
                self.model.customer_state_count["Q"] -= 1
            else:
                self.model.customer_state_count["QS"] -= 1
            self._is_served = True

        self.model.updateCashStatus()
        self.model.updateCashOpen()
        self.increment_steps()

    def increment_steps(self):
        self._num_of_steps += 1

    def get_steps(self):
        return self._num_of_steps

    def get_min_position(self, destination):
        """
        Get alternative cells to go in to move closer to destination
        """
        pos = self.pos
        if (destination != list(self.pos)):
            possible_steps = self.model.grid.get_neighborhood(
                self.pos,
                moore=True,
                include_center=True)
            minDist = 999
            num_adj_agents = 0

            prob_stationary = False
            for p in possible_steps:
                if self.model.grid.is_cell_empty(p) == False and p != self.pos and int(
                        self.model.grid_matrix[p[1]][p[0]]) != -1:
                    num_adj_agents += 1
                    if (self.model._id_test == 3 and p == (20, 15) and any(self._lanes_list) == 1):
                        prob_stationary = True
                dist = np.sqrt(
                    np.power(destination[0] - p[0], 2) + np.power(destination[1] - p[1], 2))
                if (dist < minDist and self.model.grid.is_cell_empty(p) == True):
                    if (p[1] < 5 and self._state_move == "M" and any(self._lanes_list) == 1):
                        pass
                    else:
                        minDist = dist
                        pos = p

            # probability to move in alternative position
            if (self.pos != pos):
                possible_steps.remove(pos)
                # get alternative
                minDist = 999
                alt = pos
                for p in possible_steps:
                    dist = np.sqrt(
                        np.power(destination[0] - p[0], 2) + np.power(destination[1] - p[1], 2))
                    if (dist < minDist and self.model.grid.is_cell_empty(p) == True):
                        if (p[1] < 5 and self._state_move == "M" and any(self._lanes_list) == 1):
                            pass
                        else:
                            minDist = dist
                            alt = p

                coin = np.random.rand()
                if (coin <= 0.05):
                    pos = self.pos
                elif (0.05 <= coin <= 0.07 and num_adj_agents == 0):
                    pos = alt
                elif (0.05 < coin <= 0.25 and num_adj_agents > 0):
                    pos = alt
                # p1=10% p2=5% stationary=85%
                if (prob_stationary == True):
                    if (coin <= 0.85):
                        pos = self.pos
                    elif (coin <= 0.90):
                        pos = alt
        return pos

    def decide_movement_snake(self):
        """
        Decide movement (Snake mode) towards cashe registers at the end of shopping phase
        """
        cashOpened = self.model.positionCashOpened
        j = 0
        is_cash_choosed = False

        # iterate on open cash registers
        while j < len(cashOpened):
            c = [2, 5, 8, 11, 14, 17, 20, 23]
            position_to_cycle = 0
            while position_to_cycle < 8:
                if c[position_to_cycle] == cashOpened[j]:
                    position_in_cashiers = position_to_cycle
                position_to_cycle += 1
            if self.model.cashiers[position_in_cashiers]._is_occupied == False and is_cash_choosed == False:
                self.model.cashiers[position_in_cashiers]._is_occupied = True
                self._chosen_cash_pos = cashOpened[j]
                is_cash_choosed = True
            j = j + 1

    def decideMovement(self):
        """
        Decide movement (Default mode) towards cashe registers at the end of shopping phase
        """

        is_an_admissible_zone = self.go_to_the_chashier()
        if is_an_admissible_zone != 999:
            return is_an_admissible_zone

        if self.pos[0] + 1 in self.model.positionCashOpened:
            pos_now = (self.pos[0], self.pos[1] + 1)
            if pos_now[0] < self.model.gridWidth and pos_now[1] < self.model.gridHeight:
                if self.model.grid.is_cell_empty(pos_now) == False or self.pos[1] < 5:
                    return 0

        distance = []
        decision = []
        j = 0
        while j < len(self.model.positionCashOpened):
            distance.append(self.model.positionCashOpened[j] - 1 - self.pos[0])
            j = j + 1
        j = 0
        avg = self.model.avarageQueue()
        while j < len(self.model.positionCashOpened):
            if distance[j] < 0:
                if self.model.cashStatus[j] < avg and self.model.cashStatus[j] != 0:
                    decision.append(
                        distance[j] * -1 + self.model.cashStatus[j])
                elif self.model.cashStatus[j] == 0:
                    decision.append(distance[j])
                else:
                    decision.append(
                        distance[j] * -1 + self.model.cashStatus[j] * 2)
            elif distance[j] == 0:  # Preferisco sempre la cassa per la quale sono sempre in coda
                if self.model.cashStatus[j] == 0:
                    decision.append(-999)
                else:
                    decision.append(self.model.cashStatus[j])
            else:
                if self.model.cashStatus[j] < avg and self.model.cashStatus[j] != 0:
                    decision.append(distance[j] + self.model.cashStatus[j])
                elif self.model.cashStatus[j] == 0:
                    decision.append(1)
                else:
                    decision.append(distance[j] + self.model.cashStatus[j] * 2)
            j = j + 1

        if (len(decision) != 0):
            min_to_move = min(decision)
        index_to_move = decision.index(min_to_move)

        if (distance[index_to_move] < 0 and self._last_move != 1):
            return -1
        elif (distance[index_to_move] < 0 and self._last_move == 1):
            count = 0
            while count < len(distance):
                if distance[count] > 0:
                    return 1
                count += 1
            return -1

        elif (distance[index_to_move] == 0):
            return 0
        elif distance[index_to_move] > 0 and self._last_move != -1:
            return 1
        elif distance[index_to_move] > 0 and self._last_move == -1:
            count = 0
            while count < len(distance):
                if distance[count] < 0:
                    return -1
                count += 1
            return 1

    def go_to_the_chashier(self):
        if self.pos[1] >= 11 and self.pos[1] <= 19:
            pos_now = (self.pos[0], self.pos[1] - 1)
            if self.model.grid.is_cell_empty(pos_now) == True:
                return 0
            else:
                pass

        if self._steps != -1:
            self._steps = self._steps + 1
        # Se sono in cima a uno scaffale, scendo giù verso la zona delle casse e prendo la decisione quando mi trovo in quella zona
        if self._steps > 10:
            self._lock_movement == False
            self._steps = -1

        if self._lock_movement == True and self._steps in range(0, 10):
            pos_now = (self.pos[0], self.pos[1] - 1)
            if self.model.grid.is_cell_empty(pos_now) == True:
                return 0
            else:
                pass

        if self.pos[0] - 1 in self.model.positionCashOpened:
            if self.pos[1] >= 18:
                pos_now = (self.pos[0], self.pos[1] - 1)
                pos_now2 = (self.pos[0], self.pos[1] - 2)
                pos_now3 = (self.pos[0], self.pos[1] - 3)
                if self.model.grid.is_cell_empty(pos_now) == False and self.model.grid.is_cell_empty(
                        pos_now2) == False and self.model.grid.is_cell_empty(pos_now3) == False:
                    pos_now = (self.pos[0] - 1, self.pos[1])
                    if self.model.grid.is_cell_empty(pos_now) == True:
                        self._lock_movement = True
                        self._steps = 0
                        return -1
                    else:
                        self._steps = 0
                        self._lock_movement = True
                        return 1
        return 999


class CustomerModel(Model):
    """
    Supermarket model
    """

    def __init__(self, N, C, Q, activeCash, gridWidth, gridHeight, mode, idTest, NW):
        self.customer_state_count = {"E": 0, "M": 0, "Q": 0, "QS": 0, "PAY": 0, "PAYED": 0}
        self.agent_status = []
        self.positionCashOpened = []
        self.initialCashOpened = []
        # global entryPoints
        self.entryPoints = []
        self.cashStatus = []
        self._queue_type = Q
        plt.ion()
        self.registerClose = []
        self.activeCash = activeCash

        self.gridWidth = gridWidth
        self.gridHeight = gridHeight

        self.running = True
        self.num_cash_initial = activeCash
        self.num_agents = N
        self.num_cashiers = 8
        self.lock_close = False
        self.cashiers = []
        self.workerPlaces = []

        self.num_workers = NW
        if self.num_cash_initial + self.num_workers > 8:
            self.num_workers = 8 - self.num_cash_initial

        # Analysis
        self._sum_tot_steps = 0
        self._avg_steps = 0
        self._num_agents_sample = 0

        self._test_case_desc = ""

        # Chart
        if self._queue_type == "Default":
            self.datacollector = DataCollector(
                {"Agent in queue": lambda m: m.customer_state_count["Q"],
                 "Agent in move": lambda m: m.customer_state_count["M"],
                 "Agent in payment": lambda m: m.customer_state_count["PAY"],
                 "Agent paid": lambda m: m.customer_state_count["PAYED"],
                 "Agent in entry": lambda m: m.customer_state_count["E"],
                 "Avg customer steps": lambda m: m._avg_steps,
                 "Test case description": lambda m: m._test_case_desc})
        else:
            self.datacollector = DataCollector(
                {"Agent in queue": lambda m: m.customer_state_count["QS"],
                 "Agent in move": lambda m: m.customer_state_count["M"],
                 "Agent in payment": lambda m: m.customer_state_count["PAY"],
                 "Agent paid": lambda m: m.customer_state_count["PAYED"],
                 "Agent in entry": lambda m: m.customer_state_count["E"],
                 "Avg customer steps": lambda m: m._avg_steps,
                 "Test case description": lambda m: m._test_case_desc})

        # PLOT
        self._enabled_analysis = True
        self._steps = []
        self._entry_agents = []
        self._moving_agents = []
        self._queue_agents = []
        self._queue_agentsQS = []
        self._payed_agents = []
        if mode == True:
            self._mode = "test"
        else:
            self._mode = "sim"
            self._test_case_desc = "Test mode <span style=\"color:red\">disabled</span>. Enable it to run test cases"
        self._id_test = int(idTest[len(idTest) - 1])
        if (self._mode == "test"):
            if (self._id_test == 0):
                self.agentToSpawn = 2
                self.num_agents = 2
                self._test_case_desc = "Test conflict in movement between two agents"
            if (self._id_test == 1):
                self.agentToSpawn = 10
                self.num_agents = 10
                self._test_case_desc = "Test conflict in movement between two opposite groups of agents"
            if (self._id_test == 2):
                self.agentToSpawn = 8
                self.num_agents = 8
                self._test_case_desc = "Test behavior of group of agents moving to the same target position"
            if (self._id_test == 3):
                self.agentToSpawn = 3
                self.num_agents = 3
                self._test_case_desc = "Test behavior of three agents picking up products from the same shelf"
            if (self._id_test == 4):
                self.agentToSpawn = 6
                self.num_agents = 6
                self._test_case_desc = "Test agents behavior moving to cash registers"

        else:
            self.agentToSpawn = self.num_agents
        self.grid = SingleGrid(gridWidth, gridHeight, False)
        self.schedule = RandomActivation(self)
        rnd = np.zeros((gridWidth, gridHeight))
        p = 0
        k = 0

        if C != activeCash:
            # randomize which cash registers are open
            c = [2, 5, 8, 11, 14, 17, 20, 23]
            self.positionCashOpened = random.sample(c, activeCash)
        else:
            # all cash registers are open
            c = []
            self.positionCashOpened = list(range(8))

        i = 0
        while i < len(self.positionCashOpened):
            self.initialCashOpened.append(self.positionCashOpened[i])
            i = i + 1
        self.grid_matrix = []

        # Matrix containing grid cells info
        if (self._queue_type == "Default"):
            self.grid_matrix_read = open(
                'supermarketqueuing/config/matrix.txt').read()
        if (self._queue_type == "Snake"):
            self.grid_matrix_read = open(
                'supermarketqueuing/config/matrix_snake.txt').read()
        self.grid_matrix = [item.split()
                            for item in self.grid_matrix_read.split('\n')[::-1]]

        self.heatmap = [item.split()
                        for item in self.grid_matrix_read.split('\n')[::-1]]
        # matrix for heatmap
        for i in range(0, len(self.heatmap)):
            for j in range(0, len(self.heatmap[0])):
                self.heatmap[i][j] = 0

        for row in range(len(self.grid_matrix)):
            for column in range(len(self.grid_matrix[row])):
                if self.grid_matrix[row][column] == '-1':

                    obstacle = ObstacleAgent(self)

                    if (column <= 24 and row == 5) or (column == 0 and row == 0 or row == 24):
                        obstacle._orientation = "orizontal"
                    if column == 25 and row == 5:
                        obstacle._orientation = "conjunction"

                    self.schedule.add(obstacle)
                    self.grid.place_agent(obstacle, (column, row))

        # Creating supermarket entrance points
        for entry_x in range(1, 4):
            currentEntryPosition = (C + entry_x, 0)
            self.entryPoints.append(currentEntryPosition)

        # Creating cash registers
        cash_x = 2

        while cash_x <= 24:
            cash = CashierAgent(self)
            worker = WorkerAgent(self)

            if cash_x in self.positionCashOpened:
                cash.setActive(1)
                cash.initial = True
                worker._active = True

            self.schedule.add(cash)
            self.grid.place_agent(cash, (cash_x, 0))
            self.cashiers.append(cash)

            self.schedule.add(worker)
            self.grid.place_agent(worker, (cash_x + 1, 0))
            self.workerPlaces.append(worker)

            cash_x += 3

        self.updateCashOpen()

        for w in self.positionCashOpened:
            self.cashStatus.append(0)
            self.registerClose.append(0)

        self.datacollector.collect(self)

    def updateCashOpen(self):
        """
        Update global vision about which cash registers are currently open
        """

        self.positionCashOpened.clear()

        for cash in self.cashiers:

            # Cash register opened
            if cash._active_status == 1 and cash.pos[0] not in self.positionCashOpened:
                self.positionCashOpened.append(cash.pos[0])

                for worker in self.workerPlaces:
                    if worker.pos[0] == cash.pos[0] + 1:
                        worker._active = True

    def updateCashStatus(self):
        """
        Update cash registers status
        """
        self.cashStatus.clear()
        # resetto coda
        i = 0
        while i < len(self.positionCashOpened):
            self.cashStatus.append(0)
            i = i + 1
        i = 0
        while i < len(self.positionCashOpened):
            inc_pos = -1
            queue = 0
            pos = (self.positionCashOpened[i] - 1, inc_pos)
            cycle = True
            while cycle:
                queue += 1
                inc_pos = inc_pos + 1
                pos = (self.positionCashOpened[i] - 1, inc_pos)
                if inc_pos >= self.gridHeight:
                    break

                if self.grid.is_cell_empty(pos) == False:
                    cycle = True
                else:
                    cycle = False
            self.cashStatus[i] = queue - 1
            i = i + 1

    def avarageQueue(self):
        """
        Utility to keep trace of average queue length
        """
        i = 0
        avg = 0
        while i < len(self.cashStatus):
            avg = avg + self.cashStatus[i]
            i = i + 1
        if len(self.cashStatus) != 0:
            avg = avg / len(self.cashStatus)
        print(self.cashStatus)
        return avg

    def step(self):
        self.lock_close = False
        '''Advance the model by one step.'''
        # Controllo spawn agenti
        self.schedule.step()
        self.datacollector.collect(self)

        print(self.cashStatus)

        if (self._mode != "test"):
            coin = np.random.rand()
            if coin <= 0.75:
                prob_to_spawn = 0.06
            elif coin <= 0.90:
                prob_to_spawn = 0.02
            else:
                prob_to_spawn = 0.2
            p1 = 1 if np.random.rand() <= prob_to_spawn else 0
            p2 = 1 if np.random.rand() <= prob_to_spawn else 0
            p3 = 1 if np.random.rand() <= prob_to_spawn else 0
            enteringPositions = [p1, p2, p3]
            for entryPosition in self.entryPoints:
                if enteringPositions[self.entryPoints.index(entryPosition)] == 1 and self.grid.is_cell_empty(
                        entryPosition) == True and self.agentToSpawn != 0:
                    agent = CustomerAgent(self)
                    self.schedule.add(agent)
                    self.grid.place_agent(agent, entryPosition)
                    self.agentToSpawn -= 1
        else:
            if (self._id_test == 0 and self.agentToSpawn > 0):
                # spawn first agent
                agent = CustomerAgent(self)
                agent._test_marking = "TR"
                self.schedule.add(agent)
                self.grid.place_agent(agent, (14, 8))
                # spawn second agent
                self.agentToSpawn -= 2
                agent = CustomerAgent(self)
                agent._test_marking = "TL"
                self.schedule.add(agent)
                self.grid.place_agent(agent, (15, 8))
            elif (self._id_test == 1 and self.agentToSpawn > 0):
                for count in range(1, 6):
                    # spawn first agent
                    agent = CustomerAgent(self)
                    agent._test_marking = "TR"
                    self.schedule.add(agent)
                    if self.grid.is_cell_empty((14, 5 + count)):
                        self.grid.place_agent(agent, (14, 5 + count))
                    # spawn second agent
                    self.agentToSpawn -= 2
                    agent = CustomerAgent(self)
                    agent._test_marking = "TL"
                    self.schedule.add(agent)
                    if self.grid.is_cell_empty((15, 5 + count)):
                        self.grid.place_agent(agent, (15, 5 + count))
            elif (self._id_test == 2 and self.agentToSpawn > 0):
                for count in range(1, self.num_agents + 1):
                    # spawn first agent
                    agent = CustomerAgent(self)
                    self.schedule.add(agent)
                    rand_pos = (random.randint(0, 11), random.randint(6, 10))
                    while not self.grid.is_cell_empty(rand_pos):
                        rand_pos = (random.randint(0, 11), random.randint(6, 10))
                    self.grid.place_agent(agent, rand_pos)
                    self.agentToSpawn -= 1
            elif (self._id_test == 3 and self.agentToSpawn > 0):
                i = 1
                for count in range(1, self.num_agents + 1):
                    # spawn first agent
                    agent = CustomerAgent(self)
                    self.schedule.add(agent)
                    rand_pos = (random.randint(0, 11), random.randint(6, 9))
                    while not self.grid.is_cell_empty(rand_pos):
                        rand_pos = (random.randint(4, 8), random.randint(8, 9))
                    agent._test_marking = "T3-" + str(i)
                    self.grid.place_agent(agent, rand_pos)
                    self.agentToSpawn -= 1
                    i += 1
            elif (self._id_test == 4 and self.agentToSpawn > 0):
                curr_pos = (2, 10)  # below first lanes
                offset_pos_x = 0
                for count in range(1, self.num_agents + 1):
                    # spawn each agents below each lanes
                    agent = CustomerAgent(self)
                    self.schedule.add(agent)
                    curr_pos = (curr_pos[0] + offset_pos_x, curr_pos[1])
                    if self.grid.is_cell_empty(curr_pos):
                        self.grid.place_agent(agent, curr_pos)
                    self.agentToSpawn -= 1
                    offset_pos_x = 5

        avarage = self.avarageQueue()
        count_state_q = self.customer_state_count['Q'] + self.customer_state_count['PAY']
        count_state_qs = self.customer_state_count['QS'] + self.customer_state_count['PAY']
        count_state_different = self.customer_state_count['E'] + self.customer_state_count['M']
        count_state_different_qs = self.customer_state_count['E'] + self.customer_state_count['M']

        avarage_Q = count_state_q / len(self.positionCashOpened)
        if (self.customer_state_count['M'] != 0):
            RMA_cash = len(self.positionCashOpened) * 40 / self.customer_state_count['M']
        else:
            RMA_cash = 999
        if (count_state_different_qs != 0):
            RQA_s = count_state_qs / count_state_different_qs
        else:
            RQA_s = 0
        if count_state_qs != 0:
            RIQ_s = len(
                self.positionCashOpened) * 4 / count_state_qs  # rapporto tra casse aperte e numero di agenti che vogliono pagare
        else:
            RIQ_s = 999

        if (count_state_different != 0):
            # Numero di agenti con stato Q diviso agenti totali nell'ambiente
            RQA = count_state_q / count_state_different
        else:
            RQA = 0

        if count_state_q != 0:
            RIQ = len(
                self.positionCashOpened) * 4 / count_state_q  # rapporto tra casse aperte e numero di agenti che vogliono pagare
        else:
            RIQ = 999
        if count_state_q != 0:
            RMQ = self.customer_state_count['M'] / (count_state_q)
        else:
            RMQ = 0

        if self._queue_type == "Snake" and len(self.positionCashOpened) < (
                self.num_workers + self.num_cash_initial):

            # BIAS 6% of agents as congestion queue threshold
            if RIQ_s < 0.6 or (RMA_cash < 1):
                new_reg_open = False
                while not new_reg_open:
                    c = [2, 5, 8, 11, 14, 17, 20, 23]
                    rnd_cash = random.sample(c, 1)
                    while rnd_cash[0] in self.positionCashOpened and len(self.positionCashOpened) <= 8:
                        rnd_cash = random.sample(c, 1)
                    position_to_cycle = 0
                    while position_to_cycle < 8:
                        if c[position_to_cycle] == rnd_cash[0]:
                            position_in_cashiers = position_to_cycle
                        position_to_cycle += 1

                    if rnd_cash[0] not in self.positionCashOpened:
                        self.cashiers[position_in_cashiers].setActive(1)
                        new_reg_open = True
                        self.positionCashOpened.append(rnd_cash[0])
                        self.cashStatus.append(0)
                        self.registerClose.append(0)

        if self._queue_type == "Default":
            if (RIQ < 1 and avarage_Q > 4.5 and RQA != 0 and len(self.positionCashOpened) < (
                    self.num_workers + self.num_cash_initial) and 0 not in self.cashStatus):
                new_reg_open = False
                while not new_reg_open:
                    c = [2, 5, 8, 11, 14, 17, 20, 23]
                    rnd_cash = random.sample(c, 1)
                    while rnd_cash[0] in self.positionCashOpened and len(self.positionCashOpened) <= 8:
                        rnd_cash = random.sample(c, 1)
                    position_to_cycle = 0
                    while position_to_cycle < 8:
                        if c[position_to_cycle] == rnd_cash[0]:
                            position_in_cashiers = position_to_cycle
                        position_to_cycle += 1

                    if rnd_cash[0] not in self.positionCashOpened:
                        self.cashiers[position_in_cashiers].setActive(1)
                        new_reg_open = True
                        self.positionCashOpened.append(rnd_cash[0])
                        self.cashStatus.append(0)
                        self.registerClose.append(0)
        j = 0
        while j < len(self.positionCashOpened):

            if self.cashStatus[j] == 0:
                self.registerClose[j] = self.registerClose[j] + 1
            else:
                self.registerClose[j] = 0
            j = j + 1

        i = 0

        # print(str(avarage) + "  " + str(RQA) + "  " + str(RIQ))
        if len(self.positionCashOpened) > self.num_cash_initial:
            while i < len(self.positionCashOpened):
                if self.positionCashOpened[i] not in self.initialCashOpened and self._queue_type == "Default":
                    c = [2, 5, 8, 11, 14, 17, 20, 23]
                    position_to_cycle = 0
                    while position_to_cycle < 8:
                        if c[position_to_cycle] == self.positionCashOpened[i]:
                            position_in_cashiers = position_to_cycle
                        position_to_cycle += 1
                    print(self.registerClose[i])
                    if self.registerClose[i] > 25 or (
                            avarage_Q < 3 and (((RQA > 0.6 or RQA < 0.2) and RIQ > 1.5 and RMQ < 2.5) or RQA == 0)) and \
                            self.cashiers[
                                position_in_cashiers].now_open == False and self.cashStatus[
                        i] <= 3:  # Se cassa è stata vuota per almeno 12 passi e la media è molto bassa
                        if self.lock_close == False:
                            self.closeCashier(i)
                            self.lock_close = True
                elif self.positionCashOpened[i] not in self.initialCashOpened and self._queue_type == "Snake":
                    c = [2, 5, 8, 11, 14, 17, 20, 23]
                    position_to_cycle = 0
                    while position_to_cycle < 8:
                        if c[position_to_cycle] == self.positionCashOpened[i]:
                            position_in_cashiers = position_to_cycle
                        position_to_cycle += 1
                    if (self.registerClose[i] > 10 and (RQA_s > 0.6 or (RQA_s < 0.2 and RMA_cash > 2)) and RIQ_s > 1 and
                            self.cashiers[position_in_cashiers].now_open == False and self.cashiers[
                                position_in_cashiers]._is_occupied == False):
                        self.closeCashier(i)
                i = i + 1
        self.updateCashStatus()
        if self._enabled_analysis == True:
            self.analysis()

        print("[TEST] Avg step " + str(self._avg_steps))
        print("[TEST] Num of sample " + str(self._num_agents_sample))

    def closeCashier(self, i):
        """
        Logic to be perfomed when to close a cash register
        """
        position_to_cycle = 0
        c = [2, 5, 8, 11, 14, 17, 20, 23]
        while position_to_cycle < 8:
            if c[position_to_cycle] == self.positionCashOpened[i]:
                position_in_cashiers = position_to_cycle
            position_to_cycle += 1
        self.cashiers[position_in_cashiers].initToClose(0)
        pos_to_compare = (self.positionCashOpened[i] - 1, 0)
        if (self._queue_type == "Snake"):
            c = [2, 5, 8, 11, 14, 17, 20, 23]
            position_to_cycle = 0
            while position_to_cycle < 8:
                if c[position_to_cycle] == self.positionCashOpened[i]:
                    position_in_cashiers = position_to_cycle
                position_to_cycle += 1
            if (self.grid.is_cell_empty(pos_to_compare) and self.cashiers[
                position_in_cashiers]._is_occupied == False):
                self.cashiers[position_in_cashiers].setClose(-1)

        if self._queue_type == "Default":
            self.cashiers[position_in_cashiers].initToClose(0)
        j = 0
        prov_position = []
        while j < len(self.positionCashOpened):
            prov_position.append(self.positionCashOpened[j])
            j = j + 1
        self.positionCashOpened.clear()
        j = 0
        while j < len(prov_position):
            if prov_position[j] != i:
                self.positionCashOpened.append(prov_position[j])
            j = j + 1
        self.updateCashStatus()
        self.registerClose.clear()
        j = 0
        while j < len(prov_position):
            self.registerClose.append(0)
            j = j + 1

    def analysis(self):
        """
        Analysis
        """
        # PLOT agents entry "E" flag
        self._entry_agents.append(self.customer_state_count["E"])
        self._moving_agents.append(self.customer_state_count["M"])
        if (self._queue_type == "Snake"):
            self._queue_agentsQS.append(self.customer_state_count["QS"])
        else:
            self._queue_agents.append(self.customer_state_count["Q"])
        self._payed_agents.append(self.customer_state_count["PAYED"])
        if (self._payed_agents[len(self._payed_agents) - 1] >= self.num_agents and self._mode != "test"):
            total_steps = self.schedule.steps + 1
            hm = np.flip(self.heatmap, 0)
            hm = hm.astype(np.uint16)
            purchasing_heatmap = np.zeros(hm.shape)
            queue_heatmap = np.zeros(hm.shape)
            for r in range(hm.shape[0]):
                for c in range(hm.shape[1]):
                    if self._queue_type == "Default":
                        if (c >= 25 or (r <= 17 and c <= 25)):
                            purchasing_heatmap[r][c] = hm[r, c]
                            queue_heatmap[r][c] = 0
                        else:
                            purchasing_heatmap[r][c] = 0
                            queue_heatmap[r][c] = hm[r, c]
                    else:
                        if (c >= 25 or (r <= 18 and c <= 25)):
                            purchasing_heatmap[r][c] = hm[r, c]
                            queue_heatmap[r][c] = 0
                        else:
                            purchasing_heatmap[r][c] = 0
                            queue_heatmap[r][c] = hm[r, c]

            plt.imshow(purchasing_heatmap, cmap='inferno')
            plt.axis("off")
            plt.title("Steps required: " + str(total_steps))
            plt.savefig(
                'results and plot/HEATMAP/Purchasing/heatmap' + self._queue_type + str(self.num_agents) + 'C' + str(
                    self.activeCash) + '.png')
            plt.close()
            plt.imshow(queue_heatmap, cmap='inferno')
            plt.axis("off")
            plt.title("Steps required: " + str(total_steps))
            plt.savefig('results and plot/HEATMAP/Queue/heatmap' + self._queue_type + str(self.num_agents) + 'C' + str(
                self.activeCash) + '.png')
            plt.close()
            np.savetxt('heatmap.txt', self.heatmap, fmt='%1i')
            if (self._queue_type == "Snake"):
                self._entry_agents = [x for x in self._entry_agents if x != 0]
                with open(
                        'results and plot/CSV/entrySnake' + str(self.num_agents) + 'C' + str(self.activeCash) + '.csv',
                        'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._entry_agents)):
                        filewriter.writerow([self._entry_agents[i], i + 1])

                self._moving_agents = [
                    x for x in self._moving_agents if x != 0]
                with open(
                        'results and plot/CSV/movingSnake' + str(self.num_agents) + 'C' + str(self.activeCash) + '.csv',
                        'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._moving_agents)):
                        filewriter.writerow([self._moving_agents[i], i + 1])
                self._queue_agentsQS = [
                    x for x in self._queue_agentsQS if x != 0]
                with open(
                        'results and plot/CSV/queueSnake' + str(self.num_agents) + 'C' + str(self.activeCash) + '.csv',
                        'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._queue_agentsQS)):
                        filewriter.writerow([self._queue_agentsQS[i], i + 1])
                self._payed_agents = [x for x in self._payed_agents if x != 0]
                with open(
                        'results and plot/CSV/payedSnake' + str(self.num_agents) + 'C' + str(self.activeCash) + '.csv',
                        'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._payed_agents)):
                        filewriter.writerow([self._payed_agents[i], i + 1])

            else:
                self._entry_agents = [x for x in self._entry_agents if x != 0]
                with open('results and plot/CSV/entryDefault' + str(self.num_agents) + 'C' + str(
                        self.activeCash) + '.csv',
                          'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._entry_agents)):
                        filewriter.writerow([self._entry_agents[i], i + 1])

                self._moving_agents = [
                    x for x in self._moving_agents if x != 0]
                with open('results and plot/CSV/movingDefault' + str(self.num_agents) + 'C' + str(
                        self.activeCash) + '.csv',
                          'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._moving_agents)):
                        filewriter.writerow([self._moving_agents[i], i + 1])
                self._queue_agents = [x for x in self._queue_agents if x != 0]
                with open('results and plot/CSV/queueDefault' + str(self.num_agents) + 'C' + str(
                        self.activeCash) + '.csv',
                          'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._queue_agents)):
                        filewriter.writerow([self._queue_agents[i], i + 1])
                self._payed_agents = [x for x in self._payed_agents if x != 0]
                with open('results and plot/CSV/payedDefault' + str(self.num_agents) + 'C' + str(
                        self.activeCash) + '.csv',
                          'w+') as csvfile:
                    filewriter = csv.writer(csvfile, delimiter=',',
                                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    for i in range(0, len(self._payed_agents)):
                        filewriter.writerow([self._payed_agents[i], i + 1])
            self._enabled_analysis = False
            self.running = False
        if (self._mode == "test"):
            total_steps = self.schedule.steps + 1

            if (((self._id_test == 0 or self._id_test == 1) and total_steps > 18) or (
                    self._id_test == 2 and total_steps > 28)):
                if (self._id_test == 0):
                    self.heatmap[8][8] = 0
                    self.heatmap[8][20] = 0
                if (self._id_test == 1):
                    for i in range(6, 11):
                        self.heatmap[i][7] = 0
                        self.heatmap[i][21] = 0
                if (self._id_test == 2):
                    self.heatmap[8][21] = 0
                hm = np.flip(self.heatmap, 0)
                hm = hm.astype(np.uint16)
                plt.imshow(hm, cmap='inferno')
                plt.axis("off")
                plt.title("Steps required: " + str(total_steps))
                plt.savefig(
                    'results and plot/HEATMAP/UnitCase/heatmapTest' + str(self._id_test) + '.png')
                plt.close()
                self._enabled_analysis = False
                self.running = False
            if self._payed_agents[len(self._payed_agents) - 1] >= self.num_agents:
                self.heatmap[15][20] = 0
                hm = np.flip(self.heatmap, 0)
                hm = hm.astype(np.uint16)
                purchasing_heatmap = np.zeros(hm.shape)
                queue_heatmap = np.zeros(hm.shape)
                for r in range(hm.shape[0]):
                    for c in range(hm.shape[1]):
                        if self._queue_type == "Default":
                            if (c >= 25 or (r <= 17 and c <= 25)):
                                purchasing_heatmap[r][c] = hm[r, c]
                                queue_heatmap[r][c] = 0
                            else:
                                purchasing_heatmap[r][c] = 0
                                queue_heatmap[r][c] = hm[r, c]
                        else:
                            if (c >= 25 or (r <= 18 and c <= 25)):
                                purchasing_heatmap[r][c] = hm[r, c]
                                queue_heatmap[r][c] = 0
                            else:
                                purchasing_heatmap[r][c] = 0
                                queue_heatmap[r][c] = hm[r, c]
                plt.imshow(purchasing_heatmap, cmap='inferno')
                plt.axis("off")
                plt.title("Steps required: " + str(total_steps))
                plt.savefig(
                    'results and plot/HEATMAP/UnitCase/heatmap' + self._queue_type + 'Test' + str(
                        self._id_test) + 'Purchasing.png')
                plt.close()
                plt.imshow(queue_heatmap, cmap='inferno')
                plt.axis("off")
                plt.title("Steps required: " + str(total_steps))
                plt.savefig(
                    'results and plot/HEATMAP/UnitCase/heatmap' + self._queue_type + 'Test' + str(
                        self._id_test) + 'Queue.png')
                plt.close()
                self._enabled_analysis = False
                self.running = False