from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.UserParam import UserSettableParameter
from mesa.visualization.modules import CanvasGrid
from mesa.visualization.modules.ChartVisualization import ChartModule
from mesa.visualization.modules.PieChartVisualization import PieChartModule
from mesa.visualization.modules.TextVisualization import TextElement

from supermarketqueuing.model import CustomerModel, CashierAgent, ObstacleAgent, CustomerAgent, WorkerAgent

# Simulation mode
mode = UserSettableParameter('checkbox', 'Test mode', False)
id_test = UserSettableParameter('choice', 'Unit test', value='Unit case #0',
                                choices=['Unit case #0', 'Unit case #1','Unit case #2','Unit case #3','Unit case #4'])

# Grid size
gridHeight = 25
gridWidth = 30

gridLanesWidth = 10
gridLanesHeight = 5

numCashiers = 25

# Queuing mechanism
queueType = UserSettableParameter('choice', 'Queue', value='Snake',
                                  choices=['Default', 'Snake'])

activeCash = UserSettableParameter(
    'slider', 'Cash Registers starting open', 1, 1, 8)


numAgents=UserSettableParameter('slider', 'Number of Agents', 1, 1, 500) # BIAS 25% of supermarket surface
numWorkers = UserSettableParameter('slider', 'Number of Workers', 4, 1, 7)

# Visualization Grid parameter
x_resolution = 100

def agent_portrayal(agent):
    portrayal = {"Shape": "circle",
                 # "scale": 0.6,
                 "Color": "blue",
                 "Filled": "true",
                 "Layer": 0,
                 "r": 0.5
                 }

    if type(agent) == WorkerAgent:
        portrayal["Shape"] = "circle"
        portrayal["r"] = 0.5
        if agent._active:
            portrayal["Color"] = "orange"
        else:
            portrayal["Color"] = "white"

    if type(agent) == CustomerAgent:
        if agent._state_move == "M":
            if not mode.value or id_test.value == "Unit case #4":
                portrayal["Color"] = "green"
            else:
                if agent._test_marking == "TL":
                    portrayal["Color"] = "blue"
                if agent._test_marking == "TR":
                    portrayal["Color"] = "red"
                if agent._test_marking == "T3-1":
                    portrayal["Color"] = "blue"
                if agent._test_marking == "T3-2":
                    portrayal["Color"] = "red"
                if agent._test_marking == "T3-3":
                    portrayal["Color"] = "green"

        if agent._state_move == "Q" or agent._state_move == "PAY" or agent._state_move == "QS":
            portrayal["Color"] = "gray"

    if type(agent) == CashierAgent:
        if agent._active_status == 1:
            portrayal["Color"] = "green"
        elif agent._active_status == -1:
            portrayal["Color"] = "red"
        else:
            portrayal["Color"] = "orange"
        portrayal["Shape"] = "rect"
        portrayal["w"] = 1
        portrayal["h"] = 1

    if type(agent) == ObstacleAgent:
        portrayal["Color"] = "black"
        portrayal["Shape"] = "rect"
        if agent._orientation == "vertical":
            portrayal["w"] = 0.3
            portrayal["h"] = 1
        elif agent._orientation == "orizontal":
            portrayal["w"] = 1
            portrayal["h"] = 0.3
        elif agent._orientation == "conjunction":
            portrayal["Shape"] = "circle"
            portrayal["r"] = 0.5
            # portrayal["w"] = 0.5
            # portrayal["h"] = 0.3

    return portrayal


class AvgStepsTextElement(TextElement):
    '''
    Display a text count of how many happy agents there are.
    '''

    def __init__(self):
        pass

    def render(self, model):
        return "<p style=\"font-size:18px\"><b>Average customer steps:</b> " + "%.2f" % (model._avg_steps) + "</p>"


class CustomerStateTextElement(TextElement):
    '''
    Count the number of spawned agents for each states
    '''

    def __init__(self):
        pass

    def render(self, model):
        str_to_render = ""
        if model._queue_type == "Default":
            str_to_render = "State in move: " + str(model.customer_state_count["M"]) + "<br/> State in payment: " + str(
                model.customer_state_count["PAY"]) + "<br/> State paid: " + str(
                model.customer_state_count["PAYED"]) + "<br/> State in queue: " + str(model.customer_state_count["Q"]) + "<br/> State in entry: " + str(model.customer_state_count["E"])
        else:
            str_to_render = "State in move: " + str(model.customer_state_count["M"]) + "<br/> State in payment: " + str(
                model.customer_state_count["PAY"]) + "<br/> State paid: " + str(
                model.customer_state_count["PAYED"]) + "<br/> State in queue: " + str(model.customer_state_count["QS"]) + "<br/> State in entry: " + str(model.customer_state_count["E"])

        return str_to_render

class TestCaseDescriptionTextElement(TextElement):
    '''
    Text element to visualize which mode (simulation/testing) is enabled
    '''

    def __init__(self):
        pass
    
    def render(self, model):
        if not mode.value: # sim mode
            str_to_render = "<br/><p style=\"font-size:18px;\"><b>Simulation mode</b> " + str(model._test_case_desc) + "</p>"
        else:
            str_to_render = "<br/><p style=\"font-size:18px;\"><b>Testing mode</b> " + str(model._test_case_desc) + "</p>"

        return str_to_render

grid = CanvasGrid(agent_portrayal, gridWidth, gridHeight,
                  20 * gridWidth, 20 * gridHeight)

chart_element = ChartModule([{"Label": "Agent in queue", "Color": "#AA0000"},
                             ], data_collector_name='datacollector')

piechart_element = PieChartModule([{"Label": "Agent in queue", "Color": "#808080"},
                                   {"Label": "Agent in move", "Color": "#009933"},
                                   {"Label": "Agent in payment", "Color": "#ffa500"},
                                   {"Label": "Agent paid", "Color": "#ff0000"},
                                   {"Label": "Agent in entry", "Color": "#0033CC"}], 300, 300,
                                  data_collector_name='datacollector')

avg_text_element = AvgStepsTextElement()
state_text_element = CustomerStateTextElement()
test_case_desc_text_element = TestCaseDescriptionTextElement()

server = ModularServer(CustomerModel,
                       [grid, test_case_desc_text_element, avg_text_element, chart_element, piechart_element, state_text_element],
                       "Supermarket simulation",
                       {"mode": mode, "idTest": id_test, "N": numAgents, "C": numCashiers, "Q": queueType, "activeCash": activeCash,
                        "gridWidth": gridWidth, "gridHeight": gridHeight, "NW": numWorkers})