%%CONFIG
clear all;
close all;

num_agents='264';
num_cash='1';
file=csvread(strcat('entryDefault',num_agents,'C',num_cash,'.csv'));
yed=file(1:size(file,1),1);
xed=file(1:size(file,1),2);
% file=csvread(strcat('entrySnake',num_agents,'C',num_cash,'.csv'));
% yes=file(1:size(file,1),1);
% xes=file(1:size(file,1),2);

file=csvread(strcat('movingDefault',num_agents,'C',num_cash,'.csv'));
ymd=file(1:size(file,1),1);
xmd=file(1:size(file,1),2);
% file=csvread(strcat('movingSnake',num_agents,'C',num_cash,'.csv'));
% yms=file(1:size(file,1),1);
% xms=file(1:size(file,1),2);

file=csvread(strcat('queueDefault',num_agents,'C',num_cash,'.csv'));
yqd=file(1:size(file,1),1);
xqd=file(1:size(file,1),2);
% file=csvread(strcat('queueSnake',num_agents,'C',num_cash,'.csv'));
% yqs=file(1:size(file,1),1);
% xqs=file(1:size(file,1),2);

file=csvread(strcat('payedDefault',num_agents,'C',num_cash,'.csv'));
ypd=file(1:size(file,1),1);
xpd=file(1:size(file,1),2);
% file=csvread(strcat('payedSnake',num_agents,'C',num_cash,'.csv'));
% yps=file(1:size(file,1),1);
% xps=file(1:size(file,1),2);

%%
%PLOT

%PLOT ENTRY Default VS Snake X agents
plot(xed,yed,'LineWidth',2)
hold on
% plot(xes,yes,'LineWidth',2)
legend({'Entry agents Default'},'FontSize',14,'Location','eastoutside')
grid on
title(strcat('Entry agents Default'));
xlabel('Step');
ylabel('Num agents')
figure

%PLOT MOVING Default VS Snake X agents
plot(xmd,ymd,'LineWidth',2)
hold on
% plot(xms,yms,'LineWidth',2)
legend({'Moving agents Default'},'FontSize',14,'Location','eastoutside')
grid on
title(strcat('Moving agents Default'));
xlabel('Step');
ylabel('Num agents')
figure

%PLOT QUEUE Default VS Snake X agents
plot(xqd,yqd,'LineWidth',2)
% hold on
% plot(xqs,yqs,'LineWidth',2)
legend({'Queue agents Default'},'FontSize',14,'Location','eastoutside')
grid on
title(strcat('Queue agents Default'));
xlabel('Step');
ylabel('Num agents')
figure 

%PLOT PAYED Default VS Snake X agents
plot(xpd,ypd,'LineWidth',2)
% hold on
% plot(xps,yps,'LineWidth',2)
legend({'Payed agents Default'},'FontSize',14,'Location','eastoutside')
grid on
title(strcat('Payed agents Default'));
xlabel('Step');
ylabel('Num agents')
