# Complex System project - Supermarket queue simulator
## Minimum requirements
To run the project, it is necessary to install the following programs/libraries/frameworks:
* *Python 3.x.x*, you can download needed files from the official website
* *Mesa*, you can consult user guide following this link <https://mesa.readthedocs.io/en/master/index.html>

## Command Line Arguments
* Using command-line prompt, point to project root directory saved locally on your terminal
* From the root of the project, run command `py run.py` or `python run.py` (i.e. in Anaconda prompt) or `mesa runserver`

## Simulation & WebUI
The interface is composed of different areas:
* Region #1 (right side of navabar). In this area there are simulation control buttons to start, pause and reset the current simulation session.
* Region #2 (top-right side in UI body). In this area there is the grid in which simulations are perfored. Above the grid there is a slider to set the frame per second (informally. the simulation speed).
  An information string below the grid reports which mode is enabled (simlation/test) 
* Region #3 (left side in UI body). In this area there are user-settable simulation parameters:
	* **Test mode** - enabled/disable test mode
	* **Unit test** - this parameter allows to select the unit test to be performed. *Note that Test mode needs to be enabled to run test cases*.
	* **Number of agents** - this parameter allows to set the number of agents of the simulation
	* **Queue** - this parameter set the queue type between Snake mode and Default mode
	* **Cash Registers starting open** - this parameter set the number of cash registers initially opened at the beginning of the simulation
	* **Number of Workers** - this parameter set the number of staff agents that can take over during the simulation as additional cashiers in support of the already existing ones 
* Region # 4 (right side, below the Region #3). Region to perform dynamic analysis during simulations.

## Known issues
### Modifying simulation parameters
If you want to modify simulation parameters, you have to stop eventually running simulation sessions, using **Stop** (or **Step**) button in Region #1. To render effective changes on simulation parameters, it is necessary to reset simulator, using **Reset** button in Region #1.

### UI rendering
During testing phase, it is observed different graphic rendering according to the browser used. If some of such troubles occur, update your browser to the latest version or change it to another one.
It is recommended to use the latest version of Google Chrome or Microsoft Edge.
